#include <Arduino.h>
#include <MIDI.h>
#include "config.h"

/**
 * @brief countNotes defines the struct to hold the amount of keys pressed of the same note
 * 
 */
struct countNotes
{
    #ifdef USE_C
    int16_t count_C;
    #endif
    #ifdef USE_Cs
    int16_t count_Cs;
    #endif
    #ifdef USE_D
    int16_t count_D;
    #endif
    #ifdef USE_Ds
    int16_t count_Ds;
    #endif
    #ifdef USE_E
    int16_t count_E;
    #endif
    #ifdef USE_F
    int16_t count_F;
    #endif
    #ifdef USE_Fs
    int16_t count_Fs;
    #endif
    #ifdef USE_G
    int16_t count_G;
    #endif
    #ifdef USE_Gs
    int16_t count_Gs;
    #endif
    #ifdef USE_A
    int16_t count_A;
    #endif
    #ifdef USE_As
    int16_t count_As;
    #endif
    #ifdef USE_B
    int16_t count_B;
    #endif
};

/**
 * @brief A callback function that should be called whenever a noteOn message is received. It increments the count of the note that has been pressed
 * 
 * @param channel is the channel on wich the message has been received and can have one of the values 1-16
 * @param pitch is the note that has been played, starts at C(-2) = 0 up to G(9) = 127. 
 *      On a standard piano, the range is from A(0) = 21 up to C(8) = 108
 * @param velocity the force at which a note is played 0-99
 */
void handleNoteOn(byte channel, byte pitch, byte velocity);

/**
 * @brief A callback function that should be called whenever a noteOff message is received. It decrements the count of the note that has been released
 * 
 * @param channel is the channel on wich the message has been received and can have one of the values 1-16
 * @param pitch is the note that has been played, starts at C(-2) = 0 up to G(9) = 127. 
 *      On a standard piano, the range is from A(0) = 21 up to C(8) = 108
 * @param velocity the force at which a note is played 0-99
 */
void handleNoteOff(byte channel, byte pitch, byte velocity);

/**
 * @brief returns whether at least one of this note is being pressed
 * 
 * @param note is the note that has been played, starts at C(-2) = 0 up to G(9) = 127. 
 *      On a standard piano, the range is from A(0) = 21 up to C(8) = 108
 * @return true the count is at least 1
 * @return false the count is 0
 */
bool isPressed(int16_t note);

/**
 * @brief interrupt called by the InterruptTimer and updates the LEDs
 * 
 */
void updateLED();

/**
 * @brief This is called whenever a note is being pressed or released
 * 
 * @param pitch is the note that has been played, starts at C(-2) = 0 up to G(9) = 127. 
 *      On a standard piano, the range is from A(0) = 21 up to C(8) = 108
 * @param isNoteOn boolean: NoteOn->true, NoteOff->false
 */
void updateNote(byte pitch, bool isNoteOn);

/**
 * @brief sets the counters for each note to 0 and sets the LED pins to OUTPUT
 * 
 */
void initLEDs();