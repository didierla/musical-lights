#include "main.h"

/**
 * @brief Construct a new midi create instance object
 * 
 * Serial1 on the Teensy3.5 are on pins 0(RX1) and 1(TX1)
 * This is different from the Serial which goes through the debug port
 */
MIDI_CREATE_INSTANCE(HardwareSerial, Serial1, MIDI);

/**
 * @brief Create a timer object to update the LEDs at regular intervals
 * 
 */
IntervalTimer updateLedTmr;

/**
 * @brief notes holds the amount of keys pressed of the same note for every note
 * 
 */
countNotes notes;

void setup()
{

  initLEDs();

  MIDI.begin(MIDI_CHANNEL_OMNI);
  MIDI.setHandleNoteOn(handleNoteOn);
  MIDI.setHandleNoteOff(handleNoteOff);

  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  digitalWrite(LED_BUILTIN, LOW);
  updateLedTmr.begin(updateLED, 1000);
}

void loop()
{
  MIDI.read(1);
}

void handleNoteOn(byte channel, byte pitch, byte velocity)
{
  updateNote(pitch, true);
}

void handleNoteOff(byte channel, byte pitch, byte velocity)
{
  updateNote(pitch, false);
}

void updateLED()
{
  #ifdef USE_C
  digitalWrite(LED_C, isPressed(notes.count_C));
  #endif
  #ifdef USE_Cs
  digitalWrite(LED_Cs, isPressed(notes.count_Cs));
  #endif
  #ifdef USE_D
  digitalWrite(LED_D, isPressed(notes.count_D));
  #endif
  #ifdef USE_Ds
  digitalWrite(LED_Ds, isPressed(notes.count_Ds));
  #endif
  #ifdef USE_E
  digitalWrite(LED_E, isPressed(notes.count_E));
  #endif
  #ifdef USE_F
  digitalWrite(LED_F, isPressed(notes.count_F));
  #endif
  #ifdef USE_Fs
  digitalWrite(LED_Fs, isPressed(notes.count_Fs));
  #endif
  #ifdef USE_G
  digitalWrite(LED_G, isPressed(notes.count_G));
  #endif
  #ifdef USE_Gs
  digitalWrite(LED_Gs, isPressed(notes.count_Gs));
  #endif
  #ifdef USE_A
  digitalWrite(LED_A, isPressed(notes.count_A));
  #endif
  #ifdef USE_As
  digitalWrite(LED_As, isPressed(notes.count_As));
  #endif
  #ifdef USE_B
  digitalWrite(LED_B, isPressed(notes.count_B));
  #endif
}

void initLEDs()
{
  pinMode(LED_BUILTIN, OUTPUT);
  #ifdef USE_C
  notes.count_C = 0;
  pinMode(LED_C, OUTPUT);
  digitalWrite(LED_C, HIGH);
  delay(500);
  digitalWrite(LED_C, LOW);
  #endif
  #ifdef USE_Cs
  notes.count_Cs = 0;
  pinMode(LED_Cs, OUTPUT);
  digitalWrite(LED_Cs, HIGH);
  delay(500);
  digitalWrite(LED_Cs, LOW);
  #endif
  #ifdef USE_D
  notes.count_D = 0;
  pinMode(LED_D, OUTPUT);
  digitalWrite(LED_D, HIGH);
  delay(500);
  digitalWrite(LED_D, LOW);
  #endif
  #ifdef USE_Ds
  notes.count_Ds = 0;
  pinMode(LED_Ds, OUTPUT);
  digitalWrite(USE_Ds, HIGH);
  delay(500);
  digitalWrite(USE_Ds, LOW);
  #endif
  #ifdef USE_E
  notes.count_E = 0;
  pinMode(LED_E, OUTPUT);
  digitalWrite(LED_E, HIGH);
  delay(500);
  digitalWrite(LED_E, LOW);
  #endif
  #ifdef USE_F
  notes.count_F = 0;
  pinMode(LED_F, OUTPUT);
  digitalWrite(LED_F, HIGH);
  delay(500);
  digitalWrite(LED_F, LOW);
  #endif
  #ifdef USE_Fs
  notes.count_Fs = 0;
  pinMode(LED_Fs, OUTPUT);
  digitalWrite(LED_Fs, HIGH);
  delay(500);
  digitalWrite(LED_Fs, LOW);
  #endif
  #ifdef USE_G
  notes.count_G = 0;
  pinMode(LED_G, OUTPUT);
  digitalWrite(LED_G, HIGH);
  delay(500);
  digitalWrite(LED_G, LOW);
  #endif
  #ifdef USE_Gs
  notes.count_Gs = 0;
  pinMode(LED_Gs, OUTPUT);
  digitalWrite(LED_Gs, HIGH);
  delay(500);
  digitalWrite(LED_Gs, LOW);
  #endif
  #ifdef USE_A
  notes.count_A = 0;
  pinMode(LED_A, OUTPUT);
  digitalWrite(LED_A, HIGH);
  delay(500);
  digitalWrite(LED_A, LOW);
  #endif
  #ifdef USE_As
  notes.count_As = 0;
  pinMode(LED_As, OUTPUT);
  digitalWrite(LED_As, HIGH);
  delay(500);
  digitalWrite(LED_As, LOW);
  #endif
  #ifdef USE_B
  notes.count_B = 0;
  pinMode(LED_B, OUTPUT);
  digitalWrite(LED_B, HIGH);
  delay(500);
  digitalWrite(LED_B, LOW);
  #endif
}

void updateNote(byte pitch, bool isNoteOn) {
  int note = pitch % 12;
  switch (note) {
    #ifdef USE_C
    case 0:
      if (isNoteOn) notes.count_C++;
      else notes.count_C--;
      break;
    #endif
    #ifdef USE_Cs
    case 1:
      if (isNoteOn) notes.count_Cs++;
      else notes.count_Cs--;
      break;
    #endif
    #ifdef USE_D
    case 2:
      if (isNoteOn) notes.count_D++;
      else notes.count_D--;
      break;
    #endif
    #ifdef USE_Ds
    case 3:
      if (isNoteOn) notes.count_Ds++;
      else notes.count_Ds--;
      break;
    #endif
    #ifdef USE_E
    case 4:
      if (isNoteOn) notes.count_E++;
      else notes.count_E--;
      break;
    #endif
    #ifdef USE_F
    case 5:
      if (isNoteOn) notes.count_F++;
      else notes.count_F--;
      break;
    #endif
    #ifdef USE_Fs
    case 6:
      if (isNoteOn) notes.count_Fs++;
      else notes.count_Fs--;
      break;
    #endif
    #ifdef USE_G
    case 7:
      if (isNoteOn) notes.count_G++;
      else notes.count_G--;
      break;
    #endif
    #ifdef USE_Gs
    case 8:
      if (isNoteOn) notes.count_Gs++;
      else notes.count_Gs--;
      break;
    #endif
    #ifdef USE_A
    case 9:
      if (isNoteOn) notes.count_A++;
      else notes.count_A--;
      break;
    #endif
    #ifdef USE_As
    case 10:
      if (isNoteOn) notes.count_As++;
      else notes.count_As--;
      break;
    #endif
    #ifdef USE_B
    case 11:
      if (isNoteOn) notes.count_B++;
      else notes.count_B--;
      break;
    #endif
    default:
      break;
  }
}

bool isPressed(int16_t note) {
  return (bool)note;
}
