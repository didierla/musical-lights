/**
 * @brief USE_CUSTOM_KEYS, USE_ALL_KEYS, USE_WHITE_KEYS, USE_BLACK_KEYS
 * 
 */
#define USE_WHITE_KEYS

/**
 * @brief MAP_CUSTOM, MAP_DEFAULT
 * 
 */
#define MAP_CUSTOM



#ifdef USE_CUSTOM_KEYS
#define USE_C
//#define USE_Cs
//#define USE_D
//#define USE_Ds
#define USE_E
//#define USE_F
//#define USE_Fs
#define USE_G
//#define USE_Gs
//#define USE_A
//#define USE_As
//#define USE_B
#endif

#ifdef USE_ALL_KEYS
#define USE_C
#define USE_Cs
#define USE_D
#define USE_Ds
#define USE_E
#define USE_F
#define USE_Fs
#define USE_G
#define USE_Gs
#define USE_A
#define USE_As
#define USE_B
#endif

#ifdef USE_WHITE_KEYS
#define USE_C
#define USE_D
#define USE_E
#define USE_F
#define USE_G
#define USE_A
#define USE_B
#endif

#ifdef USE_BLACK_KEYS
#define USE_Cs
#define USE_Ds
#define USE_Fs
#define USE_Gs
#define USE_As
#endif

#ifdef MAP_CUSTOM
#ifdef USE_C
#define LED_C 14
#endif
#ifdef USE_Cs
#define LED_Cs 34
#endif
#ifdef USE_D
#define LED_D 33
#endif
#ifdef USE_Ds
#define LED_Ds 36
#endif
#ifdef USE_E
#define LED_E 15
#endif
#ifdef USE_F
#define LED_F 34
#endif
#ifdef USE_Fs
#define LED_Fs 39
#endif
#ifdef USE_G
#define LED_G 16
#endif
#ifdef USE_Gs
#define LED_Gs 15
#endif
#ifdef USE_A
#define LED_A 35
#endif
#ifdef USE_As
#define LED_As 17
#endif
#ifdef USE_B
#define LED_B 36
#endif
#endif

#ifdef MAP_DEFAULT
#ifdef USE_C
#define LED_C 33
#endif
#ifdef USE_Cs
#define LED_Cs 34
#endif
#ifdef USE_D
#define LED_D 35
#endif
#ifdef USE_Ds
#define LED_Ds 36
#endif
#ifdef USE_E
#define LED_E 37
#endif
#ifdef USE_F
#define LED_F 38
#endif
#ifdef USE_Fs
#define LED_Fs 39
#endif
#ifdef USE_G
#define LED_G 14
#endif
#ifdef USE_Gs
#define LED_Gs 15
#endif
#ifdef USE_A
#define LED_A 16
#endif
#ifdef USE_As
#define LED_As 17
#endif
#ifdef USE_B
#define LED_B 18
#endif
#endif